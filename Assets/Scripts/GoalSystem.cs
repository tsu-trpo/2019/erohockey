﻿using UnityEngine;

public class GoalSystem : MonoBehaviour
{
    public Player Player;
    public ScoreSystem ScoreSystem;
    public Collider2D Puck;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other == Puck)
        {
            float sign = (Player == Player.one) ? 1f : -1f;
            Vector2 pos = new Vector2(0f, 2f * sign);
            Puck.transform.position = pos;

            ScoreSystem.AddScore(Player);
        }
    }   
}
