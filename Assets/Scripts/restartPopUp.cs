﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RestartPopUp : MonoBehaviour
{
    public void OnMouseDown()
    {
        SceneManager.LoadScene(Scenes.main);
    }
}

