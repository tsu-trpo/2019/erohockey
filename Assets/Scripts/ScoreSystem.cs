﻿using UnityEngine.UI;
using UnityEngine;


public class ScoreSystem : MonoBehaviour
{
    public Text[] texts;
    public GameObject RestartPopUp;

    public void AddScore(Player Player)
    {
        int index = (int)Player;
        scores[index] += 1;
        texts[index].text = scores[index].ToString();

        if(scores[index] == end)
        {
            RestartPopUp.SetActive(true);
        }
    }

    private int[] scores = { 0, 0 };
    private const int end = 7;
}
