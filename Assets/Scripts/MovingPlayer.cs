﻿using UnityEngine;

public class MovingPlayer : MonoBehaviour
{
    public Rigidbody2D Player1;
    public Rigidbody2D Player2;

    public GameObject BorderCube;

    private void Start()
    {
        Bounds bounds = BorderCube.GetComponent<Collider>().bounds;
        const float offset = 0.2f;

        BorderPlayer1.min = bounds.min;
        BorderPlayer1.max = new Vector3(bounds.max.x, -offset);

        BorderPlayer2.min = new Vector3(bounds.min.x, offset);
        BorderPlayer2.max = bounds.max;
    }

    private void Update()
    {
        Touch[] touches = Input.touches;

        foreach (Touch touch in touches)
        {
            if (touch.fingerId == fingerId_player1)
            {
                continue;
            }
            if (touch.fingerId == fingerId_player2)
            {
                continue;
            }

            Vector3 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            if (touchPos.y < 0)
            {
                tryCapture(touch.fingerId, ref fingerId_player1);
            }
            else
            {
                tryCapture(touch.fingerId, ref fingerId_player2);
            }
        }

        movePlayer(Player1, touches, ref fingerId_player1, BorderPlayer1);
        movePlayer(Player2, touches, ref fingerId_player2, BorderPlayer2);
    }

    private void movePlayer(Rigidbody2D Player, Touch[] touches, ref int? playerFingerId, Bounds BorderPlayer)
    {
        if (playerFingerId.HasValue)
        {
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(touches[playerFingerId.Value].position);
            movePlayerMouse(touchPos, Player, BorderPlayer);

            if (touches[playerFingerId.Value].phase == TouchPhase.Ended)
            {
                playerFingerId = null;
            }
        }
    }

    private void tryCapture(int fingerId, ref int? playerFingerId)
    {
        if (!playerFingerId.HasValue)
        {
            playerFingerId = fingerId;
        }
    }

#if UNITY_EDITOR
    //Для тестов!
    private void OnMouseDrag()
    {
        Vector3 Pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if(Pos.y < 0)
        {
            movePlayerMouse(Pos, Player1, BorderPlayer1);
        }
        else
        {
            movePlayerMouse(Pos, Player2, BorderPlayer2);
        }
    }
#endif

    //И это для тестов!)
    private void movePlayerMouse(Vector3 Pos, Rigidbody2D Player, Bounds BorderPlayer)
    {
        Vector2 clampedPos = new Vector2(Mathf.Clamp(Pos.x, BorderPlayer.min.x, BorderPlayer.max.x),
                                         Mathf.Clamp(Pos.y, BorderPlayer.min.y, BorderPlayer.max.y));
        Player.MovePosition(clampedPos);
    }

    private Bounds BorderPlayer1;
    private Bounds BorderPlayer2;

    private int? fingerId_player1;
    private int? fingerId_player2;
}