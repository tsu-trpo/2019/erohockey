﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour
{
    public void PlayPressed()
    {
        SceneManager.LoadScene("main");
    }

    public void ExitPressed()
    {
        Application.Quit();
    } 
}
